FROM rsnaru/myrockerspatial_build:latest
RUN Rscript -e "install.packages(c('lubridate','plyr','phenofit','patchwork'),repos='http://cran.rstudio.com/')"

RUN chown rstudio:rstudio -R /home/rstudio/
RUN chmod -R 775 /home/rstudio/
COPY ./ /home/rstudio/
RUN chmod -R 775 /home/rstudio/
RUN chown rstudio:rstudio -R /home/rstudio/
