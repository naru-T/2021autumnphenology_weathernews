import pandas as pd
import datetime
import glob

today = datetime.date.today()#今日の日付 
today_str = str(today.year) + str(today.month) + str(today.day)
list = glob.glob('./data/*.csv')

if './data/'+today_str + '.csv' not in list:
    url = 'https://weathernews.jp/s/koyo/json/migoro.json'
    df = pd.read_json(url)
    df_load = pd.read_csv('./data/data_weathernews_autumn2021.csv')

    df_load['YMD']  = df['rank']
    df_today = df_load.rename(columns={'YMD': today_str}) 

    df.to_csv( './data/' + today_str + '.csv',index=False)
    df_today.to_csv('./data/data_weathernews_autumn2021.csv',index=False)
    
